const { app, server } = require('../server')
const request = require('supertest')
const mongoose = require('mongoose')
const POSTDATA = { name: "xablau", username: "xablau", email: "xablausefourtenth@xablau.com", password: "xablau123"  }
const AUTHDATA = { email: "xablaustenth@xablau.com", password: "xablau123" }

let res = null
let TOKEN = ""

beforeAll( async () => {
    res = request(app)
    await res.post('/auth')
            .send(AUTHDATA)
            .then(res => {
                TOKEN = res.body.token
            })
})

describe('GET /user', () => {
    it('responds with json list of users', (done) => {
        res.get('/user')
            .set('Accept', 'application/json')
            .set('x-auth-token', TOKEN)
            .expect('Content-Type', /json/)
            .expect(200, done)
    })
    it('should post a new user', (done) => {
        res.post('/user')
            .send(POSTDATA)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end( (error, res) => {
                if(error) return done(error)
                done()
            })
    })
})

afterAll(async () => {
    server.close()
    await mongoose.connection.close()

})