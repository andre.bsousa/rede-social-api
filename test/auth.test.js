const { app, server } = require('../server')
const request = require('supertest')
const mongoose = require('mongoose')
const DATA = { email: "xablaustenth@xablau.com", password: "xablau123"  }

let res = null
let TOKEN = ""

beforeAll(() => {
    res = request(app)
})

describe('POST /auth', () => {
    it('should authenticate a user and responds with a TOKEN', (done) => {
        res.post('/auth')
            .send(DATA)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end( (error, res) => {
                if(error) return done(error)
                else {
                    TOKEN = res.body.token
                }
                done()
            })
    })
})

afterAll(async () => {
    server.close()
    await mongoose.connection.close()

})