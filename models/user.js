const mongoose = require('mongoose')

const opts = {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
}

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    friendships: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    }],
    topics_of_interest: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'topic'
    }],
    gender: {
        type: String,
        enum: ['Female', 'Male', 'NaN', 'No coments']
    },
    place: {
        type: String
    },
    picture: {
        type: String
    },
    short_about: {
        type: String
    },
    complete_about: {
        type: String
    },
    account_confirmed: {
        type: Boolean
    },
    privacy_term_accepted: {
        type: Boolean
    },
    skills: {
        type: [String],
        required: true
    },
    education: [
        {
          school: {
            type: String,
            required: true
          },
          degree: {
            type: String,
            required: true
          },
          fieldofstudy: {
            type: String,
            required: true
          },
          from: {
            type: Date,
            required: true
          },
          to: {
            type: Date
          },
          current: {
            type: Boolean,
            default: false
          },
          description: {
            type: String
          }
        }
    ],
    social_media: {
        facebook: String,
        instagram: String,
        twitter: String,
        github: String,
        skype: String,
        likedin: String,
    }
}, opts)

module.exports = mongoose.model('user', UserSchema)