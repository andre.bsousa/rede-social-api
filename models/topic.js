const mongoose = require('mongoose')

const TopicSchema = new mongoose.Schema({
    title: {
        type: String
    }
})

module.exports = mongoose.model('topic', TopicSchema)