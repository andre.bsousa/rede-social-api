const MSGS = {
    "EMPTY_NAME": "Por favor preencha o campo de nome",
    "GENERIC_ERROR": "Aconteceu algo inexperado",
    "INVALID_EMAIL": "Email invalido",
    "INVALID_PASSWORD": "Senha invalida, por favor insira uma senha com 5 ou mais caracteres",
    'INVALID_TOKEN': 'Token invalido',
    'NO_TOKEN': 'Sem token, autorização negada',
    'USER401': 'Credencial invalida',
    'USER404': 'Usuario não encontrado',
    'USER409': 'Usuario ja existe'
}

module.exports = MSGS