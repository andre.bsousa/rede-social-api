const express = require('express')
const router = express.Router()
const config = require('config')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const User = require('../../models/user')
const { check, validationResult } = require('express-validator')
const MSGS = require('../../messages')
const JWTSECRET = process.env.JWTSECRET || config.get('JWTSECRET')



// @route AUTH/user
// @desc Authenticate/user
// @acess Public

router.post('/', [
    check('email', 'Email invalido').isEmail(),
    check('password', 'Requer senha').exists()
], async (req, res, next) => {

    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(400).json({ erros: errors.array() })
    }

    const { email, password } = req.body
    try{

        let user = await User.findOne({ email }).select('id password email name')
        if(!user){
            return res.status(404).send({ "error": MSGS.USER404 })
        } else {

            const isMatch = await bcrypt.compare(password, user.password)

            if(!isMatch){
                return res.status(401).json({ "error": MSGS.USER401 })
            } else {
                const payload = {
                    user: {

                        id: user.id,
                        name: user.name,
                        username: user.username,
                        email: user.email
                    }
                }

                jwt.sign(payload, JWTSECRET, { expiresIn: '5 days'}, 
                (err, token) => {
                    if(err) throw err
                    res.json({ token })

                } )
            }
        }

    } catch (error){
        console.error(error.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

module.exports = router