const express = require('express')
const router = express.Router()
const Topic = require('../../models/topic')
const MSGS = require('../../messages')

// @route POST/topic
// @desc CREATE topic
// @acess Public
router.post('/', async (req, res ,next) => {
    try {
        let topic = new Topic(req.body)
        await topic.save()
        if(topic.id){
            res.json(topic)
        }
        
    } catch (error) {
        console.error(error)
        res.status(500).send({"error": MSGS.GENERIC_ERROR })        
    }
})

// @route GET/topic
// @desc LIST topics
// @acess Private
router.get('/', async (req, res , next) => {
    try {
        
        const topic = await Topic.find({})
        res.json(topic)
        
    } catch (error) {
        console.error(error)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })        
    }

})

module.exports = router