const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const auth = require('../../middlewares/auth')
const { check, validationResult } = require('express-validator')
const User = require('../../models/user')
const MSGS = require('../../messages')
const file = require('../../middlewares/file')

// @route POST/user
// @desc CREATE user
// @acess Public
router.post('/',[
    check('email', MSGS.INVALID_EMAIL).isEmail(),
    check('name', MSGS.EMPTY_NAME).not().isEmpty(),
    check('password', MSGS.INVALID_PASSWORD).isLength({min: 5})
], file,  async (req, res, next) => {
    
    try{

        let { password, email } = req.body
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({ erros: errors.array() })
        } else {
            let validarUsuario = await User.findOne({ email })
            if(validarUsuario){
                res.status(409).send({ "error": MSGS.USER409 })
            } else {
                let user = new User(req.body)
                user.picture = `user/${req.body.picture_name}`
                const salt = await bcrypt.genSalt(10)
                user.password = await bcrypt.hash(password, salt)
                await user.save()
                if(user.id){
                    res.json(user)
                }
            }
        }

    } catch (error){
        console.error(error.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }

})

// @route GET/user
// @desc List users
// @acess Private
router.get('/', auth, async (req, res, next) => {
    try {

        const user = await User.find({})
        res.json(user)
        
    } catch (error) {
        console.error(error)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
        
    }
})

// @route PATCH/user
// @desc Update user
// @acess Private
router.patch('/:userID', auth, async (req, res, next) => {
    try {
        const id = req.params.userID
        const salt = await bcrypt.genSalt(10)
        let bodyRequest = req.body

        if(bodyRequest.password){
            bodyRequest.password = await bcrypt.hash(bodyRequest.password, salt)
        }

        const update = { $set: bodyRequest }
        const user = await User.findByIdAndUpdate(id, update, { new: true })
        if(user){
            res.json(user)
        } else {
            res.status(404).send({ "error": MSGS.USER404 })
        }
        
    } catch (error) {
        console.error(error)
            res.status(500).send({ "error": MSGS.GENERIC_ERROR })        
    }
})

//@route DELETE/user
//@desc Delete user
//@acess Private
router.delete('/:userID', auth, async (req, res, next) => {
    try {
        const id = req.params.userID
        const user = await User.findByIdAndDelete({ _id: id })
        if(user){
            res.json(user)
        } else {
            res.status(404).send({ "error": MSGS.USER404 })
        }
        
    } catch (error) {
        console.error(error)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
        
    }
})

module.exports = router