const express = require('express')
const router = express.Router()
const User = require('../../models/user')
const { check, validationResult } = require('express-validator')
const auth = require('../../middlewares/auth')
const MSGS = require('../../messages')

//@route POST/education
//@desc CREATE education
//access Private
router.post('/', auth, async (req, res, next) => {
    try {
        const id = req.user.id
        const user = await User.findOneAndUpdate(id, { $push: { education: req.body }}, { new: true })
        if(user){
            res.json(user)
        } else {
            res.status(404).send({ "error": MSGS.USER404 })
        }
        
    } catch (error) {
        console.error(error)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
        
    }
})

//@route DELETE/education
//@desc Delete education
//access Private
router.delete('/', auth, async (req, res, next) => {
    try {
        const id = req.user.id
        const user = await User.findByIdAndUpdate(id, { $pull: { education: req.body }}, { new: true })
        if(user){
            res.json(user)
        } else {
            res.status(404).send({ "error": MSGS.USER404 })
        }
        
    } catch (error) {
        console.error(error)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
        
    }
})

module.exports = router