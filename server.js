const express = require('express')
const app = express()
const config = require('config')
const bodyParser = require('body-parser')
const cors = require('cors')
const PORT = process.env.PORT || config.get('PORT')
const connectDB = require('./config/db')
const fileUpload = require('express-fileupload')

//const app = require('./app')

app.use(cors())
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/uploads', express.static('uploads'))


connectDB()

app.use(fileUpload({
    createParentPath: true
}))

app.use('/', require('./routes/hello'))
app.use('/topic', require('./routes/api/topic'))
app.use('/user', require('./routes/api/user'))
app.use('/auth', require('./routes/api/auth'))
app.use('/education', require('./routes/api/education'))

const server = app.listen(PORT, () => console.log(`Rodando na porta ${PORT}`))
module.exports = { app, server }